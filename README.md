# Curso de TypeScript

## Lecciones:

* [Introducción](#module01)
    * [Lección #1 - Presentación del curso](#lesson01)
    * [Lección #2 - ¿Qué es TypeScript?](#lesson02)
    * [Lección #3 - ¿Por qué usar TypeScript?](#lesson03)
    * [Lección #4 - ¿Cuándo usar TypeScript?](#lesson04)
    * [Lección #5 - Compilación de código](#lesson05)
    * [Lección #6 - Requisitos para tomar este curso](#lesson06)
* [Primeros pasos en TS](#module02)
    * [Lección #7 - Primer programa](#lesson07)
    * [Lección #8 - Tipado dinámico y estático](#lesson08)
    * [Lección #9 - Declarar tipos de datos](#lesson09)
    * [Lección #10 - Funciones](#lesson10)
* [Programación orientada a objetos](#module03)
    * [Lección #11 - Programación orientada a objetos](#lesson11)
    * [Lección #12 - ¿Qué es la programación orientada a objetos?](#lesson12)
    * [Lección #13 - Clases y objetos](#lesson13)
    * [Lección #14 - Método constructor](#lesson14)
    * [Lección #15 - Propiedades y métodos](#lesson15)
    * [Lección #16 - Herencia](#lesson16)
    * [Lección #17 - Sobre escribir métodos](#lesson17)
    * [Lección #18 - Modificadores de acceso](#lesson18)
    * [Lección #19 - Métodos accesores](#lesson19)
    * [Lección #20 - Métodos y atributos estáticos](#lesson20)
    * [Lección #21 - Interfaces](#lesson21)
    * [Lección #22 - Clases abstractas](#lesson22)
    * [Lección #23 - Namespaces](#lesson23)
* [Tipos avanzados de datos](#module04)
    * [Lección #24 - Union Types](#lesson24)
    * [Lección #25 - Type Guards](#lesson25)
    * [Lección #26 - Intersection Types](#lesson26)
    * [Lección #27 - Type Assertions](#lesson27)
    * [Lección #28 - Type Aliases](#lesson28)
    * [Lección #29 - Tuplas](#lesson29)
    * [Lección #30 - Enums](#lesson30)
* [Decoradores y Genéricos](#module05)
    * [Lección #31 - Decoradores](#lesson31)
    * [Lección #32 - Decoradores de clase](#lesson32)
    * [Lección #33 - Decoradores de propiedades](#lesson33)
    * [Lección #34 - Decoradores de métodos](#lesson34)
    * [Lección #35 - Decoradores con argumentos](#lesson35)
    * [Lección #36 - Genéricos](#lesson36)
    * [Lección #37 - Genéricos e interfaces](#lesson37)
* [Configuración y Despedida](#module06)    
    * [Lección #38 - Editores de texto](#lesson38)
    * [Lección #39 - Tsconfig](#lesson39)
    * [Lección #40 - Compilación automática](#lesson40)
    * [Lección #41 - ¿Qué cursos tomar a continuación?](#lesson41)
    * [Lección #42 - Despedida](#lesson42)

## <a name="module01"></a> Introducción

### <a name="lesson01"></a> Lección #1 - Presentación del curso
- - -
Presentación del curso donde explica el super set de JavaScript llamado TypeScript. Es muy usado para proyectos grandes y profesionales.

La comunidad de Angular adoptó TypeScript para programar en él.

### <a name="lesson02"></a> Lección #2 - ¿Qué es TypeScript?
- - -
Typescript es un superset de JavaScript. Decimos que una tecnología es un superset de un lenguaje de programación, cuando puede ejecutar programas de la tecnología, Typescript en este caso, y del lenguaje del que es el superset, JavaScript en este mismo ejemplo. En resumen, esto significa que los programas de JavaScript son programas válidos de TypeScript, a pesar de que TypeScript sea otro lenguaje de programación.

Esta decisión fue tomada en Microsoft bajo la promesa de que las futuras versiones de Ecmascript traerían adiciones y mejoras interesantes a Javascript, esto significa que Typescript se mantiene a la vanguardia con las mejoras de JavaScript.

Además, esto permite que uno pueda integrar Typescript en proyectos existentes de JavaScript sin tener que reimplementar todo el código del proyecto en Typescript, de hecho, es común que existan proyectos que introduzcan tanto Typescript como JavaScript.

Por si fuera poco, uno de los beneficios adicionales de esta característica del lenguaje, es que pone a disposición el enorme ecosistema de librerías y frameworks que existen para JavaScript. Con Typescript podemos desarrollas aplicaciones con React, Vue, Angular, etc.

Más información en https://codigofacilito.com/articulos/typescript.

### <a name="lesson03"></a> Lección #3 - ¿Por qué usar TypeScript?
- - -
La industria de la programación es increíblemente diversa, incluso si te especializas en algún área de la programación, todos los días hay algo nuevo que aprender, una nueva tecnología, un nuevo enfoque para solucionar los problemas, etc.

De vez en cuando llega una tecnología que deslumbra tus ojos, de la que inmediatamente te enamoras e incluso te preguntas, ¿cómo es que podías trabajar sin esta tecnología? Typescript es esa clase de tecnología.

Cuando escribes un lenguaje con la intención de que éste se preste para el desarrollo de herramientas para los desarrolladores ¿qué obtienes? Un lenguaje de programación con una experiencia de desarrollo superior a otros.

Desarrollar en Typescript es increíble, prueba de ello es la cantidad de equipos de desarrollo que han integrado la herramienta, como sustituto del uso de JavaScript, entre ellos tenemos:

* Google
* Microsoft
* Basecamp
* Lyft
* Miles de empresas más

### <a name="lesson04"></a> Lección #4 - ¿Cuándo usar TypeScript?
- - -
Se recomienda usar en proyectos grandes únicamente.

### <a name="lesson05"></a> Lección #5 - Compilación de código
- - -
El código de TS se debe compilar previamente a JS para que sea interpretado por el navegador o los servidores. Este código suele ser muy limpio y el equipo de Microsoft trabaja diariamente en irlo mejorando continuamente.

### <a name="lesson06"></a> Lección #6 - Requisitos para tomar este curso
- - -
Se debe conocer previamente JavaScript a nivel intermedio.

## <a name="module02"></a> Primeros pasos en Ts

### <a name="lesson07"></a> Lección #7 - Primer programa
- - -
Para este curso usaremos la siguiente plataforma: https://www.typescriptlang.org/play

Para esta clase, escribiremos el siguiente código:

```typescript
class Speaker {
    hi(nombre: string) {
        console.log(nombre);
    }
}

let speaker: Speaker = new Speaker();

speaker.hi('Eduardo');
```

Esto será compilado a JS y se verá de la siguiente manera:

```javascript
"use strict";
class Speaker {
    hi(nombre) {
        console.log(nombre);
    }
}
let speaker = new Speaker();
speaker.hi('Eduardo');
```

Lo que mostrará en consola es Eduardo.

### <a name="lesson08"></a> Lección #8 - Tipado dinámico y estático
- - -
La principal característica de Typescript es el tipado estático. Decimos que un lenguaje es de tipado estático cuando cumple con estas características principales:

* Las variables tienen un tipo de dato.
* Los valores sólo se pueden asignar a variables del tipo correspondiente.

El interprete de JS asigna de forma dinámica los tipos de las variables, por eso decimos que es de tipado dinámico.

### <a name="lesson09"></a> Lección #9 - Declarar tipos de datos
- - -
Para declarar los tipos de datos es de la siguiente manera:
```typescript
let identificador: tipo;
// const, var y let

const identificador: tipo = valor;
```

Donde:
* identificador, es el nombre de la variable.
* tipo, es el tipo de la variable donde puede ser string, number, boolean, etc.
* valor, valor de la variable.

Otro ejemplo:

```typescript
let identificador: boolean;

let arreglo: string[];

let cursos: string[] = ["JavaScript", "Ruby"];

let comodin: any;

let comodin2; // Esto se puede configurar para que no se acepte en el compilador...
```

### <a name="lesson10"></a> Lección #10 - Funciones
- - -
Ejemplo de una declaración de una función en TypeScript:
```typescript
function suma (v1: number, v2: number): number {
    return v1 + v2;
}

let adicion = (v1: number, v2: number) => v1 + v2;

suma(2, 5);
adicion(3, 1);
```

Los parámetros opcionales pueden ser escritos de la siguiente manera:
```typescript
function opcional(nombre?: string) { }

opcional();
```

Hay que recordar que los parámetros opcionales deben estar escritos al final de la función. También podemos establecer valores por defecto. Ejemplo:
```typescript
function opcional(defecto: string = "Inicial") { }

opcional();

opcional("Eduardo");
```

Para indicar que la función no devuelva valores, podemos hacerlo de la siguiente manera:
```typescript
function crearUsuario(nombre: string, apellido: string, edad: number): void {
  // No podemos retornar nada excepto undefined o null.
}
```

Otro tipo que podemos asignar al resultado de una función es never. Este es usado cuando las funciones nunca terminan, como por ejemplo un búcle infinito o una excepción dentro del cuerpo de la función. Ejemplo:
```typescript
function arrojaError(): never {}
```

## <a name="module03"></a> Programación orientada a objetos

### <a name="lesson11"></a> Lección #11 - Programación orientada a objetos
- - -
JavaScript tiene soporte para escribir clases, lo que acercó más al lenguaje al paradigma de la POO a pesar de que el lenguaje este orientado a prototipos. En TS tendremos la habilidad de poder escribir interfaces, clases abstractas, decoradores y más.

### <a name="lesson12"></a> Lección #12 - ¿Qué es la programación orientada a objetos?
- - -
La Programación Orientada a Objetos (POO, en español; OOP, según sus siglas en inglés) es un paradigma de programación que viene a innovar la forma de obtener resultados. Los objetos se utilizan como metáfora para emular las entidades reales del negocio a modelar.

Muchos de los objetos prediseñados de los lenguajes de programación actuales permiten la agrupación en bibliotecas o librerías, sin embargo, muchos de estos lenguajes permiten al usuario la creación de sus propias bibliotecas.

Está basada en varias técnicas del sexenio: herencia, cohesión, abstracción, polimorfismo, acoplamiento y encapsulamiento.

Su uso se popularizó a principios de la década de 1990. En la actualidad, existe una gran variedad de lenguajes de programación que soportan la orientación a objetos.

### <a name="lesson13"></a> Lección #13 - Clases y objetos
- - -
No hay una diferencia marcada a la hora de definir una clase e instanciar un objeto entre TS y JS. Ejemplo:
```typescript
class Video {  }

let miVideo: Video = new Video();
```

Eso si, a la hora de instanciar un objeto debemos especificar el tipo que será el objeto. En este caso **Video**. 

### <a name="lesson14"></a> Lección #14 - Método constructor
- - -
Es un método que se ejecuta al instanciar un objeto. No se puede repetir y no retorna ningún resultado. Ejemplo:
```typescript
class Video {
    constructor(title: string) {
        console.log(title);
    }
}

let miVideo: Video = new Video("Clases y objetos de TypeScript");
```
Es muy usado para las configuraciones de los objetos antes de instanciarse.

### <a name="lesson15"></a> Lección #15 - Propiedades y métodos
- - -
De la lección anterior, definiremos algunas propiedades y métodos para poner en práctica los conocimientos adquiridos de TS. Ejemplo:
```typescript
class Video {
    title: string;

    constructor(title: string) {
        this.title = title;
    }

    printTitle(): void { console.log(this.title) }

    changeTitle(title: string): void { this.title = title; }

    geTitle(): string { return this.title; }
}

let miVideo: Video = new Video("Clases y objetos de TypeScript");

miVideo.printTitle();

miVideo.changeTitle("Demo");

console.log(miVideo.geTitle());
```

En este bloque, lo que estamos haciendo es imprimir el título definido en el método constructor y luego le estamos cambiando el nombre para imprimirlo nuevamente.

### <a name="lesson16"></a> Lección #16 - Herencia
- - -
Las herencia nos permite obtener todas las propiedades y métodos de una clase. Eso, con la intención de no repetir código, poder ampliar nuestra funcionalidad o reemplazar propiedades y métodos de la clase padre. Lo podemos realizar usando la palabra reservada **extends**. Ejemplo:

```typescript
class Video {
    title: string;

    constructor(title: string) {
        this.title = title;
    }

    play(): void { console.log("Playing"); }
    stop(): void { console.log("Stopped"); }
}

class YoutubeVideo extends Video {

}

let miVideo: YoutubeVideo = new YoutubeVideo("Clases y objetos de TypeScript");

miVideo.play();
```

En este ejemplo, estamos ejecutando un método que heredamos de la clase padre.

### <a name="lesson17"></a> Lección #17 - Sobre escribir métodos
- - -
Para reemplazar los métodos de las clases padres lo podemos hacer de la siguiente manera:

```typescript
class Video {
    title: string;

    constructor(title: string) {
        this.title = title;
    }

    play(): void { console.log("Playing"); }
    stop(): void { console.log("Stopped"); }
}

class YoutubeVideo extends Video {
    play(): void { console.log("Playing a YouTube Video"); }
}

let miVideo: YoutubeVideo = new YoutubeVideo("Clases y objetos de TypeScript");

miVideo.play();
```

Pero, si lo que queremos es extender la funcionalidad de la clase padre, podemos usar el método super. Por ejemplo:

```typescript
class Video {
    title: string;

    constructor(title: string) {
        this.title = title;
    }

    play(): void { console.log("Playing"); }
    stop(): void { console.log("Stopped"); }
}

class YoutubeVideo extends Video {
    play(): void {
        super.play();
        console.log("Playing a YouTube Video");
    }
}

let miVideo: YoutubeVideo = new YoutubeVideo("Clases y objetos de TypeScript");

miVideo.play();
```

El método super puede llamar a todos los métodos excepto el constructor. El constructor puede ser sobreescrito de la siguiente manera:

```typescript
class Video {
    title: string;

    constructor(title: string) {
        this.title = title;
    }

    play(): void { console.log("Playing"); }
    stop(): void { console.log("Stopped"); }
}

class YoutubeVideo extends Video {
    constructor(title: string) {
        super(title);
        console.log("Inicializando YouTube");
    }
    play(): void {
        super.play();
        console.log("Playing a YouTube Video");
    }
}

let miVideo: YoutubeVideo = new YoutubeVideo("Clases y objetos de TypeScript");

miVideo.play();
```

*Nota: Es obligatorio que si vamos a crear un constructor en la clase que hereda, debemos ejecutar super pero como una función y no como un objeto, pasándole todos los parámetros de entrada la hora de instanciar un objeto.*

### <a name="lesson18"></a> Lección #18 - Modificadores de acceso
- - -
Suponiendo el ejemplo de lecciones anteriores:

```typescript
class Video {
    title: string;

    constructor(title: string) {
        this.title = title;
    }
}

let miVideo: Video = new Video("Demo");

console.log(miVideo.title);
```

Esto es posible porque la propiedad title es publica, es decir, es equivalente a escribir `public title`. Esto viola el principio de encapsulamiento, ya que nuestras propiedades no deberían ser publicas, sino que debemos crear métodos accesores. Los modificadores **private** y **protected** prohíben que los atributos sean utilizados fuera de la implementación del objeto. Ejemplo:

```typescript
class Video {
    protected title: string;

    constructor(title: string) {
        this.title = title;
    }
}

let miVideo: Video = new Video("Demo");

console.log(miVideo.title);
``` 

### <a name="lesson19"></a> Lección #19 - Métodos accesores
- - -
Los métodos accesores son los getters y setters. Estos se encargan de leer y escribir los atributos. Ejemplo:

```typescript
class Video {
    private _title: string;

    constructor(title: string) {
        this._title = title;
    }

    get title(): string {
        return this._title;
    }

    set title(title: string) {
        this._title = title;
    }
}

let miVideo: Video = new Video("Demo");

console.log(miVideo.title);

miVideo.title = "Nuevo Demo";

console.log(miVideo.title);
```

Aquí si está cumpliendo con el principio de encapsulamiento, que no es más que ocultar el estado del objeto. Este es otro ejemplo más interesante:

```typescript
class User {
    private _name: string;

    private _lastName: string;

    get fullName(): string {
        return `${this._name} ${this._lastName}`;
    }

    set fullName(fullName: string) {
        let words = fullName.split(" ");
        this._name = words[0];
        this._lastName = words[1];
    }
}

let user: User = new User();

user.fullName = "Eduardo Márquez";

console.log(user.fullName);
```

### <a name="lesson20"></a> Lección #20 - Métodos y atributos estáticos
- - -
Los atributos estáticos son utilizados cuando los atributos le pertenecen a la clase y no al objeto como tal. También cuando se necesite una sola copia de esta propiedad y no en todos los objetos. Para usarlas, debemos hacer uso de la palabra reservada `static` y se accede únicamente con el nombre de la **clase**. Ejemplo:

```typescript
class Requestor {
    static url: string = "https://codigofacilito.com";

    getCourses() {
        console.log(`${Requestor.url}/cursos`);
    }

    getArticles() {
        console.log(`${Requestor.url}/articles`);
    }
}
```

También como definimos propiedades estáticas, podemos definir métodos estáticos y se usan cuando los métodos no necesitan de un estado interno. Ejemplo:

```typescript
class Requestor {
    static url: string = "https://codigofacilito.com";

    static getCourses() {
        console.log(`${Requestor.url}/cursos`);
    }

    static getArticles() {
        console.log(`${Requestor.url}/articles`);
    }
}

Requestor.getCourses();
```

### <a name="lesson21"></a> Lección #21 - Interfaces
- - -
*Interfaces, y a veces llamadas firmas, es el mecanismo que usa Typescript para definir tipos en las clases.* ¿Qué significa esto? Significa que, solo queremos definir las propiedades y métodos que van a ser visibles en el mundo exterior sin ninguna implementación (sin ninguna clase). Por ejemplo:

```typescript
interface Asset {
    x: number;
    y: number;
    width: number;
    height: number;
    getCoords(): string;
}

class Hero implements Asset {
    x: number;
    y: number;
    width: number;
    height: number;

    getCoords(): string {
        return "";
    }
}

class Bullet implements Asset {
    x: number;
    y: number;
    width: number;
    height: number;

    getCoords(): string {
        return "";
    }
}

class Enemy implements Asset {
    x: number;
    y: number;
    width: number;
    height: number;

    getCoords(): string {
        return "";
    }
}

class SpaceBullet extends Bullet {}

let spaceBullet: Asset = new SpaceBullet();

class Collisions {
    static check(obj: Asset, object2: Asset) {
        // Validar que exista forma de comparar los elementos
        // Compare object with object2
    }
}
```

### <a name="lesson22"></a> Lección #22 - Clases abstractas
- - -
En algunas situaciones tenemos métodos y propiedades comunes a un conjunto de clases, podemos agrupar dichos métodos y propiedades en una clase abstracta. La diferencia entre una clase normal y una abstracta es que no se pueden instanciar.

Son muy similares a las interfaces con la diferencia de que los métodos de la clase abstracta si se debe definir su funcionalidad. También, pueden existir métodos abstractos que son muy similares a los métodos que definíamos en las interfaces. Ejemplo:

```typescript
abstract class Asset {
    x: number;
    y: number;
    width: number;
    height: number;
    getCoords(): string {
        return `${this.x},${this.y}`;
    };

    abstract move(speed: number): boolean;
}

class Hero extends Asset {
    move(speed: number) {
        return true;
    }
}
```

Al heredar de Asset, perdemos la capacidad de heredar de otra clase... Es por eso, que es importante saber que definir en ese momento, si una clase abstracta o una interfaz.

### <a name="lesson23"></a> Lección #23 - Namespaces
- - -
Un espacio de nombres (técnica y correctamente definido como **namespace**), en su acepción más simple, es un conjunto de nombres en el cual todos los nombres son únicos. Es muy útil para diferenciar clases, propiedades y métodos que poseen nombres genéricos y no colisionen con el de otra librería. Ejemplo:

```typescript
namespace CF {
    export class YouTube { }
    export const url: string = "https://codigofacilito.com";
    const privado: string = "123";
}

let video: CF.YouTube = new CF.YouTube();
```

## <a name="module04"></a> Tipos avanzados de datos

### <a name="lesson24"></a> Lección #24 - Union types
- - -
Consiste en unificar varios tipos en una variable o una función. Ejemplo:

```typescript
let age: number | string;

age = 20;

age = 'hola';

age.toString();
```

Pero esto vuelve más insegura nuestra aplicación, porque no sabemos que tipo vamos a recibir en esa variable o método.

### <a name="lesson25"></a> Lección #25 - Type Guards
- - -
Un guardia de tipo consiste en evaluar que clase de tipo es la variable o la función. En este ejemplo, estamos recibiendo un argumento edad que puede ser del tipo string o del tipo number. Lo que estamos realizando so dos funciones que nos permite distinguir si el argumento de entrada es un número o una cadena de texto:

```typescript
function isNumber(obj: number | string): obj is number {
    return typeof obj === 'number';
}

function isString(obj: number | string): obj is string {
    return typeof obj === 'string';
}

function printAge(age: number | string) {
    if (isNumber(age)) {
        // Estamos seguros de que el objeto es un número
    } else {
        // Estamos seguros de que el objeto es una cadena
    }
}
```

Esto puede simplificarse de la siguiente manera:

```typescript
function printAge(age: number | string) {
    if (typeof age === 'number') {
        // Estamos seguros de que el objeto es un número
    } else {
        age.charAt(0);
        // Estamos seguros de que el objeto es una cadena
    }
}
```

Hay que recordar que cuando usamos Ts, ciertos métodos no existen en un tipo u otro, como por ejemplo el método charAt.

### <a name="lesson26"></a> Lección #26 - Intersection Types
- - -
Los tipos de intersección nos permite combinar los dos tipos en una sola variable. Ejemplo:

```typescript
class User {
    name: string;
}

class Admin {
    permissions: number;
    getPermission() { }
}

let user: User & Admin;

user.name = "Uriel";

user.permissions = 5;

user.getPermission();

user = new User();
```

Pero, al querer instanciar una clase, Ts no lo permite puesto que la variable tiene la combinación de las dos clases. Eso lo resolveremos en la siguiente lección.

### <a name="lesson27"></a> Lección #27 - Type Assertions
- - -
Los type assertions son una forma de decirle al compilador que debe confiar en ti, porque entiendes lo que estas haciendo. Un type assertion es como el type cast de otros lenguajes, pero se ejecuta sin verificar ninguna verificación o reestructurando datos. No tiene impacto durante la ejecución y es manejado exclusivamente por el compilador. TypeScript asume que el programador ha realizado las verificaciones necesarias para cerciorarse que el tipo corresponde al que dice ser.

**No debe confundirse con casting**. Entonces, para solventar el error de la lección anterior se realiza de la siguiente manera:

```typescript
class User {
    name: string;
}

class Admin {
    permissions: number;
    getPermission() { }
}

let user: User & Admin;

user = new User() as User & Admin;

user.name = "Uriel";

user.permissions = 5;

user.getPermission();
```

Otro ejemplo:

```typescript
interface AJAXSettings {
    url: string;
}

let options = {} as AJAXSettings;
options.url = "https://codigofacilito.com";
```

Esto es muy útil si queremos iniciar con un objeto vacío o cuando queremos asignar un valor a un objeto que originalmente era de otro tipo.

### <a name="lesson28"></a> Lección #28 - Type Aliases
- - -
Un alias de tipo consiste en colocar un alias a un tipo existente. Ejemplo:

```typescript
type numero = number;

let edad: numero;

edad = 20;
```

Esto solo no es muy útil de por si, pero resulta útil para los Union Types. Ejemplo:

```typescript
type NumberOrString = number | string;

let age: NumberOrString;

class User {
    name: string;
}

class Admin {
    permissions: number;
}

type UserAdmin = User & Admin;

let user: UserAdmin;
```

También podemos definir a los type aliases la estructura de una función. Ejemplo:

```typescript
type FuncString = () => string;

function executor(f: FuncString) { }

executor(() => 20);
```

En este ejemplo, Ts se quejará porque le estamos enviando una función que retorna un número. Pero, si lo realizamos de la manera correcta:

```typescript
type FuncString = () => string;

function executor(f: FuncString) { }

executor(() => "Hola");
```

Funcionará de forma correcta.

### <a name="lesson29"></a> Lección #29 - Tuplas
- - -
Cuando trabajamos con arreglos, podemos definir el tipo que tendrá las posiciones del arreglo. Ejemplo:

```typescript
let tupla: [string, number];

tupla[0] = 'Hola';
tupla[1] = 10;
```

Cuando ya queremos hacer uso de la posición 2, esta ya sería un Intersection Types... Es decir, puede recibir el tipo string o number. Ejemplo:
```typescript
let tupla: [string, number];

tupla[0] = 'Hola';
tupla[1] = 10;
tupla[2] = 20;
```

### <a name="lesson30"></a> Lección #30 - Enums
- - -
Los enumerados en TypeScript sirven para definir un tipo de datos con un conjunto de valores identificables. Estos valores pueden ser utilizados en forma de constantes para asignar como valor a variables, realizar comprobaciones en sentencias de decisión, etc. Ejemplo:

```typescript
enum Tallas {
    Chica = 1,
    Mediana = 2,
    Grande = 3,
}

enum PaymentState {
    Creado,
    Pagado,
    EnDeuda,
}

class Pedido {
    talla: number;
}

let pedido: Pedido = new Pedido();

pedido.talla = Tallas.Chica;

console.log(Tallas.Chica);

console.log(PaymentState.EnDeuda);
```

También podemos modificar el valor del cual inician los enums. Ejemplo:

```typescript
enum PaymentState {
    Creado = 1,
    Pagado,
    EnDeuda,
}

console.log(PaymentState.EnDeuda);
```

## <a name="module05"></a> Decoradores y Genéricos

### <a name="lesson31"></a> Lección #31 - Decoradores
- - -
El patrón Decorator responde a la necesidad de añadir dinámicamente funcionalidad a un Objeto. Esto nos permite no tener que crear sucesivas clases que hereden de la primera incorporando la nueva funcionalidad, sino otras que la implementan y se asocian a la primera. 

### <a name="lesson32"></a> Lección #32 - Decoradores de clase
- - -
Para decorar una clase, debemos definir una función que reciba una función. Esta función que se reciba no es más y menos que el constructor de la clase. Ejemplo:

```typescript
function Decorador(cls: Function) {
    console.log('Soy un decorador en ejecución');

    cls.prototype.className = cls.name;
}

@Decorador
class Speaker {

}

let speaker: Speaker = new Speaker();

console.log((speaker as any).className);
```

Para usar el decorador, hacemos uso de la sintaxis `@NombreDeLaFunción` antes de la clase. En este caso `@Decorador`.

### <a name="lesson33"></a> Lección #33 - Decoradores de propiedades
- - -
A diferencia de los decoradores de clases, esta recibe dos argumentos en vez de uno. El segundo argumento es el nombre de la propiedad y siempre es una cadena de texto. Mientras, el primer argumento ya no es la clase sino el prototipo de la clase, es decir `cls.prototype`. Ejemplo:

```typescript
function Decorador(clsProto: any, propertyName: string) {
    console.log('Soy un decorador en ejecución');

    clsProto.className = clsProto.constructor.name;

    console.log(propertyName);
}

class Speaker {
    @Decorador
    numero: number;
}

let speaker: Speaker = new Speaker();

console.log((speaker as any).className);
```

Para propiedades estáticas se realiza de la siguiente manera:

```typescript
function Decorador(clsProto: any, propertyName: string) {
    console.log('Soy un decorador en ejecución');

    clsProto.className = clsProto.constructor.name;

    console.log(propertyName);
}

function DecoradorStatic(cls: Function, propertyName: string) {
    console.log('Soy un decorador estático en ejecución');
}

class Speaker {
    @Decorador
    numero: number;

    @DecoradorStatic
    static otroPar: string;
}

let speaker: Speaker = new Speaker();

console.log((speaker as any).className);
```

### <a name="lesson34"></a> Lección #34 - Decoradores de métodos
- - -
Las funciones que decoran los métodos reciben 3 argumentos:
- El prototipo de la clase del método (igual que en los decoradores de las propiedades).
- El segundo, el nombre del método.
- El tercero, es el descritor de propiedades que puede ser opcional.

Ejemplo:

```typescript
function Auditable(clsProto: any, methodName: string, descriptor?: any) {
    let originalFunction = clsProto[methodName];

    let decoratedFunction = function() {
        originalFunction();
        console.log(`La función ${methodName} fue ejecutada`);
    }

    descriptor.value = decoratedFunction;

    return descriptor;
}

class Speaker {
    @Auditable
    n() {
        console.log(20);
    }
}

let speaker: Speaker = new Speaker();

speaker.n();
```

Como mención especial, también existen los decoradores con argumentos de los métodos que son más informativos que útiles.

### <a name="lesson35"></a> Lección #35 - Decoradores con argumentos
- - -
Para ello debemos usar decorator factory y consiste en retornar una función que contiene los métodos para que se ejecute el decorador. De esta manera, en el decorador puede recibir el argumento con el mensaje que estamos enviando. Ejemplo:

```typescript
function Auditable(message: string) {
    return function (clsProto: any, methodName: string, descriptor?: any) {
        let originalFunction = clsProto[methodName];

        let decoratedFunction = function() {
            originalFunction();
            console.log(message);
        }

        descriptor.value = decoratedFunction;

        return descriptor;
    }
}

class Speaker {
    @Auditable('oldN está obsoleto, hay que usar n')
    oldN() {
        console.log(10);
    }

    @Auditable('n fue ejecutado')
    n() {
        console.log(20);
    }
}

let speaker: Speaker = new Speaker();

speaker.oldN();

speaker.n();
```

### <a name="lesson36"></a> Lección #36 - Genéricos
- - -
Los tipos anys son una mala práctica porque, perdemos los beneficios del lenguaje. Cuando debemos trabajar con muchos tipos distintos, es aquí donde es muy útil los genéricos. Los genéricos son un tipo flexible que nos permite trabajar con varios tipos de datos distintos, sin perder la integridad del tipado estático. Ejemplo:

```typescript
function genericReceptor<T>(obj: T): T {
    return obj;
};

genericReceptor<string>("Uriel");

let numero = genericReceptor<number>(20);

numero = "Hola";
```

En este caso, estamos definiendo un genérico con el nombre T que puede ser lo que le establezcamos. En estos ejemplos, un string y en el siguiente un number. Notese, que al asignar número como un number, ese será su tipo y no se le podrá asignar un string.

También son muy útiles con los arreglos. Ejemplo:

```typescript
function printAll<T>(arr: T[]) {
    console.log(arr.length);
}

printAll<string>(["Hola", "Mundo"]);
printAll<number>([20, 25, 12]);
printAll<boolean>([true]);
```

También podemos usarlo con clases. Ejemplo:

```typescript
class Printer<T> {
    printAll(arr: T[]) {
        console.log(arr.length);
    }
}

let printer: Printer<number> = new Printer();

printer.printAll([12, 15]);
```

### <a name="lesson37"></a> Lección #37 - Genéricos e interfaces
- - -
También podemos limitar los genéricos usando interfaces. Ejemplo:

```typescript
interface Asset {
    x: number;
    y: number;
}

function generic<T extends Asset>(obj: T) {

}

class Point {
    x: number;
    y: number;
}

generic<Point>(new Point());
```

Cabe destacar que también podemos usar los genéricos en las interfaces de la siguiente manera:

```typescript
interface Asset<T> {
    x: number;
    y: number;
    generico: T;
}

function generic<T extends Asset<string>>(obj: T) {

}

class Point implements Asset<string> {
    x: number;
    y: number;
    generico: string;
}

class Element implements Asset<Point> {
    x: number;
    y: number;
    generico: Point;
}


generic<Point>(new Point());
```

## <a name="module06"></a> Configuración y despedida

### <a name="lesson38"></a> Lección #38 - Editores de texto
- - -
Editores o IDEs recomendados:
- VSC.
- WebStorm u otro similar.

### <a name="lesson39"></a> Lección #39 - Tsconfig
- - -
Configuración básica del Tsconfig. En la documentación podemos encontrar más opciones:
https://www.typescriptlang.org/tsconfig

### <a name="lesson40"></a> Lección #40 - Compilación automática
- - -
Para ello debemos tener instalado nodeJs, NPM e instalar de forma global tsc con el siguiente comando: `npm i tsc -g`. Una vez realizado, ya podemos compilar nuestros archivos. Para ello, nos movemos a la carpeta donde configuramos el tsconfig.json y ejecutamos en la consola el comando `tsc`. Esto, deberá generar el compilado en la carpeta establecida en el tsconfig.json.

Con la opción **watch** de tsconfig.json en true, tendremos a la consola ejecutandose constantemente en modo de visualización, donde estará pendiente de cualquier cambio para que se compile de forma automática.

También, debemos agregar la opción **rootDir** para que no cambie la estructura de carpetas al compilarse los archivos y **experimentalDecorators** por si queremos usar decoradores en nuestro proyecto.

### <a name="lesson41"></a> Lección #41 - ¿Qué cursos tomar a continuación?
- - -
Cursos que se recomiendan:
- Cualquier curso que involucre Angular.
- Curso para crear aplicaciones web progresivas.
- Curso de desarrollo de apps con Ionic.
- Curso de desarrollo de apps nativas con NativeScript.

### <a name="lesson42"></a> Lección #42 - Despedida
- - -
Fin del curso.